# Codepen URL

[Click here](https://codepen.io/pen/?template=XWXxzdQ) to open codepen.

## Before
![Alt before](/assets/before.png)

## After
![Alt after](/assets/after.png)

# Issues solved 

## Header issues
![Alt before](/assets/headerIssues.png)
```
1. Move Logo to the left and make equal paddings  in header and body  
2. Make as link, add hover and pointer  
3. Add hover and pointer, change text color, add box shadow  
4. Change text colour, add hover and pointer  
5. Add pointer on avatar  
```
## Body issues
![Alt before](/assets/bodyIssues.png)
```
6. Align label with input text, change input border on focus, set letter-spacing  
7. Add spacing, add hover and pointer  
8. Add paddings, remove background, set border, change colours
9. Set card footer to the bottom, align card elements with flex
10. Add icon to specialties, add paddings, change colour text
11. Set min width to the card
12. Remove background and shadow, add right border, add right margin, add wrapper to align center and one line with calendar icon; decrease time font size, increase font weight for facility, change text colours, align items in card
13. Change cards display type to grid, align rows and columns size regarding width
14. Set body height calc, align footer to the bottom, change footer text
```